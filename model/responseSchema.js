// Schema validation for each of the tests
const frisby = require('frisby');
const Joi = frisby.Joi;

//Schema check for saving a favourite
function validateSchemaSaveFavourite(spec) { // spec = FrisbySpec instance of the current test running
    return spec.expect('jsonTypes', {
       'id': Joi.number().required(),
       'message': Joi.string().required()

    });
}

//Schema check for getting a list of favourites
function validateSchemaGetAllFavourites(spec) { // spec = FrisbySpec instance of the current test running
    return spec.expect('jsonTypes', '*', {
      'id': Joi.number().required(),
      'created_at': Joi.date().iso().required(),
      'image': Joi.object({
                id: Joi.string().required(),
                url: Joi.string().required()
                }).required(),
      'image_id': Joi.string().required(),
      'sub_id': Joi.string().required(),
      'user_id': Joi.string().required()

    });
}

//Schema check for retrieving a specific image.
function validateSchemaGetSpecificFavourites(spec) { // spec = FrisbySpec instance of the current test running
    return spec.expect('jsonTypes',  {
      'id': Joi.number().required(),
      'created_at': Joi.date().iso().required(),
      'image': Joi.object({
                id: Joi.string().required(),
                url: Joi.string().required()
                }).required(),
      'image_id': Joi.string().required(),
      'sub_id': Joi.string().required(),
      'user_id': Joi.string().required()

    });
}

module.exports = { validateSchemaSaveFavourite, validateSchemaGetAllFavourites, validateSchemaGetSpecificFavourites };