//Generates the required payload for the tests.
const consts  = require('../resource/const');
var payloads = {};

payloads.saveFavouritePayload = {
  "image_id": consts.IMAGE_ID, // Gets the image id from the constants file
  "sub_id": consts.DEMO_USERID // Generate a unique user id for every test so that state doesn't have to maintained
 }

module.exports = payloads;