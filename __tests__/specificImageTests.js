const frisby = require('frisby');
const payloads = require('../model/payloads');
const config = require('../config/config');
const consts  = require('../resource/const');
var validate = require('../model/responseSchema.js');
const Joi = frisby.Joi;

let favId;

//Set the global headers
frisby.globalSetup({
 request: {
  headers: {
   'Content-Type': 'application/json',
   'x-api-key': consts.API_AUTH_KEY
   }
 }
});

describe("Tests with respect to a specific cat image associated to an account", () => {
//Ensure that a user account to image association is present before starting the test
 beforeAll (function() {
  jest.setTimeout(4000) // Setting Jest Timeout
  return frisby
    .post(config.catEndpoint, {body: payloads.saveFavouritePayload})
    .expect('status', 200)
    .then(function (res) {
       var data = JSON.parse(res['body']);
       favId = data.id; //Fetch the favourite Id to be used for further tests
    });
 });

//API chaining
  it('Should Retrieve a specific favourite cat image', function() {
   return frisby
    .get(config.catEndpoint +'/'+favId) //Use the favourite-d Id generated to retrieve it's properties
    .expect('status', 200)
    //Perform Schema validations and assertions for the expected response
    .use(validate.validateSchemaGetSpecificFavourites)
 });

  it('Delete a specific favourite-d cat image', function() {
   return frisby
    .del(config.catEndpoint +'/'+favId) //Use the favourite-d Id generated to delete it from the user account
    .expect('status', 200)
    .then(function (result) {
       let msg = JSON.parse(result['body'])
       //Validate that the image is successfully deleted from the user account.
       expect(msg.message).toEqual("SUCCESS");
    });
  });
})

 afterAll (function() {
   console.log("Tests with respect to a specific image completed.");
 });


