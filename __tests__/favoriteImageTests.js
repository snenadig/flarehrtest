const frisby = require('frisby');
const payloads = require('../model/payloads');
const config = require('../config/config');
const consts  = require('../resource/const');
var validate = require('../model/responseSchema.js');
const Joi = frisby.Joi;

//Set the global headers
frisby.globalSetup({
 request: {
  headers: {
     'Content-Type': 'application/json',
     'x-api-key': consts.API_AUTH_KEY
     }
   }
 });

describe("Tests with respect to a generic list of cat images", () => {

 beforeEach(function () {
        jest.setTimeout(4000) // Setting Jest Timeout
    });

 it('Should Save a favourite cat image', function() {
     return frisby
         .post(config.catEndpoint, {body: payloads.saveFavouritePayload})
         .expect('status', 200)
         //Do the schema assertions wrt to the received response
         .use(validate.validateSchemaSaveFavourite)
         .then(function (res) {
             var data = JSON.parse(res['body']);
             //Ensure that the add has happened successfully
             let message = data.message;
             expect(message).toBe("SUCCESS")

     });
 });

  it('Retrieve all favourites', function() {
      return frisby
          .get(config.catEndpoint)
          .expect('status', 200)
          //Do the schema validation/ assertions
          .use(validate.validateSchemaGetAllFavourites)
          .then(function (result) {
                let data = JSON.parse(result['body'])
                expect(data).toBeTruthy();
      });
  });
})

 afterAll (function() {
      console.log("Tests with respect to the Base Endpoint completed...");
});


