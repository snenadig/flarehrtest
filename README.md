# CAT API(Favourites) Automation Framework using Frisby.js #

This project is an API automation framework using Frisby.js to automate the CAT API for the Favourites functionality. 
The tests are aimed at the following resource paths: 

* /favourites
* /favourites/{favourite_id}

About Frisby : Frisby(https://docs.frisbyjs.com/introduction/readme) makes REST API testing easy, fast, and fun.Frisby.js comes loaded with many built-in tools for the most common things you need to test for to ensure your REST API is working as it should, and returning the correct properties, values, and types. 

### Pre-requisites ###

Pleasure ensure the following are installed and ensure the path is set on your machine.

* node (version > 10 ).
* npm 
* Code Editor of your choice.

To check that they are indeed setup, please check on the version as below :

* node -v 
* npm -v

### Installation ###

To get started with the project and run tests, please install Frisby2.0 and Jest as follows:

* Install Frisby v2.x from npm

   npm install frisby --save-dev
  
* Install Jest as Frisby uses Jest to run tests.  
  
  npm install --save-dev jest

### Folder Structure ###

The following depicts some information about the folder structure used in the below framework:

```bash
├─ config/                         # Project config for build and test processes
├─ model/                          # Contains the Payload samples and the response schema validation checks                   
├─ node_modules/                   # Project dependencies
├─ resource/                       # Data that is used or generated for the tests
├─ __tests__/                      # Workflow tests to be executed via HTTP against the Favourites Endpoint
├─ .gitignore                       # The ignore-file to prevent committing output files/folders
├─ package.json                     # NPM Package file -- Can update many of the script commands specific to your project requirements
├─ README.md                        # This README 😎
```

### Running a test ###

Once Frisby and Jest have been installed, the tests in __tests__ can be triggered from the CLI by: 

* cd <PathTOYourProject>
* jest 

### Example Result ###

The below is an example of the results:

![img.png](img.png)

Notes: 

1) Tests are focussed only on Happy Path Scenarios
2) In case an html reporter needs to be done, this can be setup via: 

* npm install jest-html-reporter --save-dev
* Make the following changes in package.json 


```bash
"jest": {
"reporters": [
  "default",
     [
      "./node_modules/jest-html-reporter",
      {
        "pageTitle": "Test Execution Report",
        "outputPath": "./report/test-report.html",
        "dateFormat": "dd/mm/yyyy HH:MM:SS"
      }
    ]
  ]
}
```
Alternatively you may also use jest-stare : https://www.npmjs.com/package/jest-stare