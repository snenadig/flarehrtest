// Storing configuration properties for the tests.
var config = {};

config.catAPI_base_url  = 'https://api.thecatapi.com'
config.catAPI_base_uriPath = '/v1/favourites'
config.catEndpoint = config.catAPI_base_url + config.catAPI_base_uriPath  ;

module.exports = config;