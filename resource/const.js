//Store all the constants required for the tests
var consts = {};

consts.API_AUTH_KEY = "DEMO-API-KEY"; //API key used for tests
consts.IMAGE_ID = "sWfuXbpSS"; // Image Id in question

//Generate a new user id
function getUserId() {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = 'Test-';
    for ( var i = 0; i < 5; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}
consts.DEMO_USERID = getUserId(); //Generate a user id for the tests

module.exports = consts;
